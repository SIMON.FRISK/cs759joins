#!/usr/bin/env zsh

#SBATCH -p instruction
#SBATCH --job-name=CS759PROJECTSimonFrisk
#SBATCH --output=JobOut.out
#SBATCH --error=JobErr.err
#SBATCH --cpus-per-task=1
#SBATCH --gres=gpu:1
#SBATCH --time=0-00:40:00

module load nvidia/cuda/11.8.0

nvcc -std=c++17 main.cpp Scan.cpp Join.cpp util.cpp HashMap.cpp CPULoopsJoin.cpp CPUHashJoin.cpp GPULoopsJoin.cu GPUHashJoin1.cu GPUHashJoin2.cu -o program_all.exe

for x in {1..5}
do
  ./program_all.exe $((10**$x)) cpunestedloops
done
for x in {1..6}
do
  ./program_all.exe $((10**$x)) cpuhash
done
for x in {1..6}
do
  ./program_all.exe $((10**$x)) gpunestedloops
done
for x in {1..6}
do
  ./program_all.exe $((10**$x)) gpuhash1
done
for x in {1..6}
do
  ./program_all.exe $((10**$x)) gpuhash2
done