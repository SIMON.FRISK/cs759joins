#include "Scan.h"
#include <iostream>
#include <stdexcept>

#define TRACEINPUT 0

Scan::Scan(int tableSize, int seed) 
: _created(0), _tableSize(tableSize), _random(seed), _generate(0, tableSize) 
{ }

Record Scan::next() {
  _created++;
  if(_created > _tableSize)
    throw std::runtime_error("Called Scan next() too many times.");

  Record r;
  r.a = _generate(_random);
  r.b = _generate(_random);

  #if TRACEINPUT
  std::cout << "Input Record: " << r.a << " " << r.b << "." << std::endl;
  #endif

  return r;
}
