#include "defs.h"
#include <unordered_map>

int outputSize(Record* s1Buffer, Record* s2Buffer, int s1Size, int s2Size) {
  std::unordered_map<int, int> numEncountersS1;
  for(int i = 0; i < s1Size; i++) {
    if(numEncountersS1.find(s1Buffer[i].a) == numEncountersS1.end())
      numEncountersS1[s1Buffer[i].a] = 0;
    numEncountersS1[s1Buffer[i].a]++;
  }

  std::unordered_map<int, int> numEncountersS2;
  for(int i = 0; i < s2Size; i++) {
    if(numEncountersS2.find(s2Buffer[i].b) == numEncountersS2.end())
      numEncountersS2[s2Buffer[i].b] = 0;
    numEncountersS2[s2Buffer[i].b]++;
  }

  int outputSize = 0;

  for (const auto& [s1Key, s1KeyEncounters] : numEncountersS1) {
    if(numEncountersS2.find(s1Key) != numEncountersS2.end()) {
      outputSize += s1KeyEncounters * numEncountersS2[s1Key];
    }
  }

  return outputSize;
}