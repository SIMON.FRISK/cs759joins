#include "defs.h"
#include "CPULoopsJoin.h"

CPULoopsJoin::CPULoopsJoin(Scan& s1, Scan& s2)
  : Join("CPU NESTED LOOPS JOIN", s1, s2)
{ }

void CPULoopsJoin::join(Scan& s1, Scan& s2) {
  // Pull all s2 input into memory buffer
  Record* s2Buffer = new Record[s2.getSize()];
  for(int i = 0; i < s2.getSize(); i++)
    s2Buffer[i] = s2.next();

  Record s1Rec;
  Record s2Rec;

  for(int i = 0; i < s1.getSize(); i++) {
    s1Rec = s1.next();
    for(int j = 0; j < s2.getSize(); j++) {
      s2Rec = s2Buffer[j];

      if(s1Rec.a == s2Rec.b) {
        Record newRecord;
        newRecord.a = s2Rec.a;
        newRecord.b = s1Rec.b;
        push_next(newRecord);
      }
    }
  }
}