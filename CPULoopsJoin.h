#pragma once
#include "defs.h"
#include "Scan.h"
#include "Join.h"

class CPULoopsJoin : public Join {
public:
  CPULoopsJoin(Scan& s1, Scan& s2);
  void join(Scan& s1, Scan& s2);
};
