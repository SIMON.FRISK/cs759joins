#include "HashMap.h"

HashMap::HashMap(int partitions) {
  _table = new std::list<Record>[partitions];
  _partitions = partitions;
}

HashMap::~HashMap() {
  delete[] _table;
}

void HashMap::add(int key, Record record) {
  int hash = key % _partitions;
  _table[hash].push_back(record);
}

std::list<Record> HashMap::get(int key) {
  int hash = key % _partitions;
  return _table[hash];
}

void HashMap::toMemoryBuffer(Record* buffer, int* splits) {
  // Put all hashmap data in consecutive buffers
  // Put records in buffer, with hash buckets consecutively after each other
  // Put the start/end index of each hash bucket in splits

  int index = 0;
  splits[0] = 0;
  int splitsIndex = 1;

  for(int i = 0; i < _partitions; i++) {
    for(auto record : _table[i]) {
      buffer[index] = record;
      index++;
    }
    splits[splitsIndex] = index;
    splitsIndex++;
  }
}