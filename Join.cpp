#include "Join.h"
#include <iostream>
#include <chrono>

using std::chrono::duration;
using std::chrono::duration_cast;
using std::chrono::high_resolution_clock;

#define TRACEOUTPUT 0

Join::Join(std::string name, Scan& s1, Scan& s2): _name(name), _produced(0), _s1(s1), _s2(s2) {
  std::cout << " ----------- " << _name << " ---------------------------------------- " << std::endl;
}

Join::~Join() {
  std::cout << _name << " produced " << _produced << " rows." << std::endl;
}

void Join::run() {
  high_resolution_clock::time_point start = high_resolution_clock::now();

  join(_s1, _s2);

  high_resolution_clock::time_point end = high_resolution_clock::now();
  double duration_time = duration_cast<duration<double, std::milli>>(end - start).count();
  std::cout << "Time: " << duration_time << "ms." << std::endl;
}

void Join::push_next(Record record) {
  _produced++;
  #if TRACEOUTPUT
  std::cout << "Output Record: " << record.a << " " << record.b << "." << std::endl;
  #endif
}