#include <iostream>
#include <string.h>
#include "defs.h"
#include "Scan.h"
#include "CPULoopsJoin.h"
#include "CPUHashJoin.h"
#include "Join.h"
#include "GPULoopsJoin.h"
#include "GPUHashJoin1.h"
#include "GPUHashJoin2.h"

int main(int argc, char** argv) {
  if(argc != 3) {
    std::cout << "Need to input 2 command line arguments, tablesize and join algorithm." << std::endl;
    return -1;
  }
  int tableSize = std::atoi(argv[1]);
  char* joinAlgorithm = argv[2];

  Scan s1(tableSize, 123131);
  Scan s2(tableSize, 456746);

  if(strcmp(joinAlgorithm, "cpunestedloops") == 0)
  {
    CPULoopsJoin join(s1, s2);
    join.run();
  }
  else if(strcmp(joinAlgorithm, "cpuhash") == 0)
  {
    CPUHashJoin join(s1, s2);
    join.run();
  }
  else if(strcmp(joinAlgorithm, "gpunestedloops") == 0)
  {
    GPULoopsJoin join(s1, s2);
    join.run();
  }
  else if(strcmp(joinAlgorithm, "gpuhash1") == 0)
  {
    GPUHashJoin1 join(s1, s2);
    join.run();
  }
  else if(strcmp(joinAlgorithm, "gpuhash2") == 0)
  {
    GPUHashJoin2 join(s1, s2);
    join.run();
  }
  else {
    std::cout << "No such join algorithm exists." << std::endl;
  }
}