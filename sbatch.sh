#!/usr/bin/env zsh

#SBATCH -p instruction
#SBATCH --job-name=CS759PROJECTSimonFrisk
#SBATCH --output=JobOut.out
#SBATCH --error=JobErr.err
#SBATCH --cpus-per-task=1
#SBATCH --gres=gpu:1
#SBATCH --time=0-00:40:00

module load nvidia/cuda/11.8.0

nvcc -std=c++17 main.cpp Scan.cpp Join.cpp util.cpp HashMap.cpp CPULoopsJoin.cpp CPUHashJoin.cpp GPULoopsJoin.cu GPUHashJoin1.cu GPUHashJoin2.cu -o program_all.exe

./program_all.exe 10000 cpunestedloops
./program_all.exe 10000 cpuhash
./program_all.exe 10000 gpunestedloops
./program_all.exe 10000 gpuhash1
./program_all.exe 10000 gpuhash2
