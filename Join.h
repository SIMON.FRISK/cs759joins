#pragma once
#include <string>
#include "defs.h"
#include "Scan.h"

class Join {
  public:
    Join(std::string name, Scan& s1, Scan& s2);
    ~Join();
    void run();
    virtual void join(Scan& s1, Scan& s2) = 0;
    void push_next(Record record);
  private:
    std::string _name;
    int _produced;
    Scan& _s1;
    Scan& _s2;
};