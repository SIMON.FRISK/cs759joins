#include "defs.h"
#include "GPULoopsJoin.h"
#include <list>
#include <unordered_map>
#include <chrono>
#include <iostream>
#include "stdio.h"

using std::chrono::duration;
using std::chrono::duration_cast;
using std::chrono::high_resolution_clock;

#define THREADS_DIM 32

#define TIMING 1

// CUDA Kernel
__global__ void LoopsJoinKernel(Record* s1Buf, int s1Len, Record* s2Buf, int s2Len, Record* dOutput, int* dOutputCount) {
  int s1Id = threadIdx.x + blockDim.x * blockIdx.x;
  int s2Id = threadIdx.y + blockDim.y * blockIdx.y;

  if(s1Id < s1Len && s2Id < s2Len) {
    Record r1 = s1Buf[s1Id];
    Record r2 = s2Buf[s2Id];

    if(r1.a == r2.b) {
      Record newRecord;
      newRecord.a = r2.a;
      newRecord.b = r1.b;
      int storeIndex = atomicAdd(dOutputCount, 1);
      dOutput[storeIndex] = newRecord;
    }
  }
}

GPULoopsJoin::GPULoopsJoin(Scan& s1, Scan& s2)
  : Join("GPU NESTED LOOPS JOIN", s1, s2)
{ }

void GPULoopsJoin::join(Scan& s1, Scan& s2) {
  high_resolution_clock::time_point startPhase1 = high_resolution_clock::now();

  // Pull input data and put in buffers
  Record* s1Buffer = new Record[s1.getSize()];
  Record* s2Buffer = new Record[s2.getSize()];
  for(int i = 0; i < s1.getSize(); i++)
    s1Buffer[i] = s1.next();
  for(int i = 0; i < s2.getSize(); i++)
    s2Buffer[i] = s2.next();

  // Allocate device memory and transfer input data
  Record* s1Buf;
  Record* s2Buf;
  cudaMalloc(&s1Buf, s1.getSize() * sizeof(Record));
  cudaMalloc(&s2Buf, s2.getSize() * sizeof(Record));
  cudaMemcpy(s1Buf, s1Buffer, s1.getSize() * sizeof(Record), cudaMemcpyHostToDevice);
  cudaMemcpy(s2Buf, s2Buffer, s2.getSize() * sizeof(Record), cudaMemcpyHostToDevice);

  // Calculate output size
  int joinOutputSize = outputSize(s1Buffer, s2Buffer, s1.getSize(), s2.getSize());

  // Allocate output buffer
  Record* dOutput;
  cudaMalloc(&dOutput, joinOutputSize * sizeof(Record));
  int outputCount;
  int* dOutputCount;
  cudaMalloc(&dOutputCount, sizeof(int));

  high_resolution_clock::time_point endPhase1 = high_resolution_clock::now();
  double duration_time_p2 = duration_cast<duration<double, std::milli>>(endPhase1 - startPhase1).count();
  #if TIMING
  std::cout << "Pulling input, memory allocation and data movement: " << duration_time_p2 << "ms" << std::endl;
  #endif

  high_resolution_clock::time_point startKernel = high_resolution_clock::now();

  dim3 threadsPerBlock(THREADS_DIM, THREADS_DIM);
  dim3 blocksInGrid((s1.getSize() / THREADS_DIM) + 1, (s2.getSize() / THREADS_DIM) + 1);

  LoopsJoinKernel<<<blocksInGrid, threadsPerBlock>>>(s1Buf, s1.getSize(), s2Buf, s2.getSize(), dOutput, dOutputCount);

  cudaDeviceSynchronize();

  high_resolution_clock::time_point endKernel = high_resolution_clock::now();
  double duration_time_kernel = duration_cast<duration<double, std::milli>>(endKernel - startKernel).count();
  #if TIMING
  std::cout << "Kernel call: " << duration_time_kernel << "ms" << std::endl;
  #endif
  high_resolution_clock::time_point startLast = high_resolution_clock::now();

  cudaMemcpy(&outputCount, dOutputCount, sizeof(int), cudaMemcpyDeviceToHost);

  // Allocate host output buffer and transfer data there
  Record* output = new Record[outputCount];
  cudaMemcpy(output, dOutput, outputCount * sizeof(Record), cudaMemcpyDeviceToHost);

  // Push output
  for(int i = 0; i < outputCount; i++)
    push_next(output[i]);

  // Free memory
  delete[] s1Buffer;
  delete[] s2Buffer;
  delete[] output;
  cudaFree(s1Buf);
  cudaFree(s2Buf);
  cudaFree(dOutput);
  cudaFree(dOutputCount);

  high_resolution_clock::time_point endLast = high_resolution_clock::now();
  double duration_last = duration_cast<duration<double, std::milli>>(endLast - startLast).count();
  #if TIMING
  std::cout << "Pushing output and freeing memory: " << duration_last << "ms" << std::endl;
  std::cout << "Execution Time: " << duration_time_kernel << "ms." << std::endl;
  #endif
}