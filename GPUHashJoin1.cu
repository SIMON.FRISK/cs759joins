#include "defs.h"
#include "GPUHashJoin1.h"
#include "HashMap.h"
#include <chrono>
#include <iostream>
#include "stdio.h"

using std::chrono::duration;
using std::chrono::duration_cast;
using std::chrono::high_resolution_clock;

#define TIMING 1

#define THREADS_DIM 32

// CUDA Kernel for hash join 1
__global__ void HashJoinKernel1(Record* s1Buf, Record* s2Buf, int* s1SplitsBuf, int* s2SplitsBuf, Record* dOutput, int* dOutputCount) {
  int s1Access = threadIdx.x + s1SplitsBuf[blockIdx.x];
  int s1PartitionEnd = s1SplitsBuf[blockIdx.x + 1];
  int s2Access = threadIdx.y + s2SplitsBuf[blockIdx.x];
  int s2PartitionEnd = s2SplitsBuf[blockIdx.x + 1];

  while(s1Access < s1PartitionEnd) {
    while(s2Access < s2PartitionEnd) {
      Record r1 = s1Buf[s1Access];
      Record r2 = s2Buf[s2Access];

      if(r1.a == r2.b) {
        Record newRecord;
        newRecord.a = r2.a;
        newRecord.b = r1.b;
        int storeIndex = atomicAdd(dOutputCount, 1);
        dOutput[storeIndex] = newRecord;
      }

      s2Access += blockDim.x;
    }
    s2Access = threadIdx.y + s2SplitsBuf[blockIdx.x];
    s1Access += blockDim.x;
  }
}

GPUHashJoin1::GPUHashJoin1(Scan& s1, Scan& s2)
  : Join("GPU HASH JOIN 1", s1, s2)
{ }

void GPUHashJoin1::join(Scan& s1, Scan& s2) {
  high_resolution_clock::time_point startPhase1 = high_resolution_clock::now();

  // Build hash tables
  // Make expected number of elements per bucket around THREADS_DIM
  int partitions = std::max(s1.getSize(), s2.getSize()) / THREADS_DIM + 1;
  HashMap s1HashMap(partitions);
  HashMap s2HashMap(partitions);
  for(int i = 0; i < s1.getSize(); i++) {
    Record record = s1.next();
    s1HashMap.add(record.a, record);
  }
  for(int i = 0; i < s2.getSize(); i++) {
    Record record = s2.next();
    s2HashMap.add(record.b, record);
  }
  Record* s1Buffer = new Record[s1.getSize()];
  int* s1Splits = new int[partitions + 1];
  s1HashMap.toMemoryBuffer(s1Buffer, s1Splits);
  Record* s2Buffer = new Record[s2.getSize()];
  int* s2Splits = new int[partitions + 1];
  s2HashMap.toMemoryBuffer(s2Buffer, s2Splits);

  high_resolution_clock::time_point endPhase1 = high_resolution_clock::now();
  double duration_time_p1 = duration_cast<duration<double, std::milli>>(endPhase1 - startPhase1).count();
  #if TIMING
  std::cout << "Building hash table: " << duration_time_p1 << "ms" << std::endl;
  #endif

  high_resolution_clock::time_point startPhase2 = high_resolution_clock::now();

  Record* s1Buf;
  Record* s2Buf;
  int* s1SplitsBuf;
  int* s2SplitsBuf;
  cudaMalloc(&s1Buf, s1.getSize() * sizeof(Record));
  cudaMalloc(&s2Buf, s2.getSize() * sizeof(Record));
  cudaMalloc(&s1SplitsBuf, (partitions + 1) * sizeof(int));
  cudaMalloc(&s2SplitsBuf, (partitions + 1) * sizeof(int));
  cudaMemcpy(s1Buf, s1Buffer, s1.getSize() * sizeof(Record), cudaMemcpyHostToDevice);
  cudaMemcpy(s2Buf, s2Buffer, s2.getSize() * sizeof(Record), cudaMemcpyHostToDevice);
  cudaMemcpy(s1SplitsBuf, s1Splits, (partitions + 1) * sizeof(int), cudaMemcpyHostToDevice);
  cudaMemcpy(s2SplitsBuf, s2Splits, (partitions + 1) * sizeof(int), cudaMemcpyHostToDevice);

  int joinOutputSize = outputSize(s1Buffer, s2Buffer, s1.getSize(), s2.getSize());

  Record* dOutput;
  cudaMalloc(&dOutput, joinOutputSize * sizeof(Record));
  int outputCount;
  int* dOutputCount;
  cudaMalloc(&dOutputCount, sizeof(int));

  high_resolution_clock::time_point endPhase2 = high_resolution_clock::now();
  double duration_time_p2 = duration_cast<duration<double, std::milli>>(endPhase2 - startPhase2).count();
  #if TIMING
  std::cout << "Memory allocation and data movement: " << duration_time_p2 << "ms" << std::endl;
  #endif
  high_resolution_clock::time_point startKernel = high_resolution_clock::now();

  dim3 threadsPerBlock(THREADS_DIM, THREADS_DIM);
  dim3 blocksInGrid(partitions);

  HashJoinKernel1<<<blocksInGrid, threadsPerBlock>>>(s1Buf, s2Buf, s1SplitsBuf, s2SplitsBuf, dOutput, dOutputCount);

  cudaDeviceSynchronize();

  high_resolution_clock::time_point endKernel = high_resolution_clock::now();
  double duration_time_kernel = duration_cast<duration<double, std::milli>>(endKernel - startKernel).count();
  #if TIMING
  std::cout << "Kernel call: " << duration_time_kernel << "ms" << std::endl;
  #endif
  high_resolution_clock::time_point startLast = high_resolution_clock::now();

  cudaMemcpy(&outputCount, dOutputCount, sizeof(int), cudaMemcpyDeviceToHost);

  Record* output = new Record[outputCount];
  cudaMemcpy(output, dOutput, outputCount * sizeof(Record), cudaMemcpyDeviceToHost);

  for(int i = 0; i < outputCount; i++)
    push_next(output[i]);

  delete[] s1Buffer;
  delete[] s2Buffer;
  delete[] s1Splits;
  delete[] s2Splits;
  delete[] output;
  cudaFree(s1Buf);
  cudaFree(s2Buf);
  cudaFree(s1SplitsBuf);
  cudaFree(s2SplitsBuf);
  cudaFree(dOutput);
  cudaFree(dOutputCount);

  high_resolution_clock::time_point endLast = high_resolution_clock::now();
  double duration_last = duration_cast<duration<double, std::milli>>(endLast - startLast).count();
  #if TIMING
  std::cout << "Pushing output and freeing memory: " << duration_last << "ms" << std::endl;
  std::cout << "Execution Time: " << duration_time_p1 +  duration_time_kernel << "ms." << std::endl;
  #endif
}
