#include "defs.h"
#include "CPUHashJoin.h"
#include <chrono>
#include <iostream>
#include "HashMap.h"

using std::chrono::duration;
using std::chrono::duration_cast;
using std::chrono::high_resolution_clock;

#define TIMING 1

CPUHashJoin::CPUHashJoin(Scan& s1, Scan& s2)
  : Join("CPU HASH JOIN", s1, s2)
{ }

void CPUHashJoin::join(Scan& s1, Scan& s2) {
  // Build hash map
  high_resolution_clock::time_point startHashBuild = high_resolution_clock::now();

  HashMap hashMap(s1.getSize());
  for(int i = 0; i < s1.getSize(); i++) {
    Record rec = s1.next();
    hashMap.add(rec.a, rec);
  }

  high_resolution_clock::time_point endHashBuild = high_resolution_clock::now();
  double duration_time_hash = duration_cast<duration<double, std::milli>>(endHashBuild - startHashBuild).count();
  #if TIMING
  std::cout << "Building hash map took " << duration_time_hash << "ms" << std::endl;
  #endif

  // Iterate through probing relation and probe on hash map
  high_resolution_clock::time_point startProbePhase = high_resolution_clock::now();

  for(int i = 0; i < s2.getSize(); i++) {
    Record probeRecord = s2.next();

    std::list<Record> list = hashMap.get(probeRecord.b);
    for(auto& buildRecord : list) {
      if(buildRecord.a == probeRecord.b) {
        Record newRecord;
        newRecord.a = probeRecord.a;
        newRecord.b = buildRecord.b;
        push_next(newRecord);
      }
    }
  }

  high_resolution_clock::time_point endProbePhase = high_resolution_clock::now();
  double duration_time_probe = duration_cast<duration<double, std::milli>>(endProbePhase - startProbePhase).count();
  #if TIMING
  std::cout << "Probing phase took " << duration_time_probe << "ms" << std::endl;
  #endif
}