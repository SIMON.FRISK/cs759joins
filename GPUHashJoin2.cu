#include "defs.h"
#include "GPUHashJoin2.h"
#include <chrono>
#include <iostream>

using std::chrono::duration;
using std::chrono::duration_cast;
using std::chrono::high_resolution_clock;

#define TIMING 1

// Kernel to take an input buffer and calculate the
// number of records in each hash partition
__global__ void HashPartitionSizeKernel(Record* buffer, int size, int* partitionSizes, int numPartitions, bool hashOnA) {
  int id = threadIdx.x + blockIdx.x * blockDim.x;
  while(id < size) {
    int hash = (hashOnA ? buffer[id].a : buffer[id].b) % numPartitions;
    atomicAdd(&partitionSizes[hash], 1);
    id += gridDim.x * blockDim.x;
  }
}

// Kernel to take an input buffer and the partition start/end indexes and populate an
// output buffer so that each partition follows consecutively
__global__ void PartitionInputKernel(Record* input, Record* output, int size, int* partitions, int* curPartitionIdx, int numPartitions, bool hashOnA) {
  int id = threadIdx.x + blockIdx.x * blockDim.x;
  while(id < size) {
    int hash = (hashOnA ? input[id].a : input[id].b) % numPartitions;
    int index = atomicAdd(&curPartitionIdx[hash], 1);
    output[index] = input[id];
    id += gridDim.x * blockDim.x;
  }
}

// Kernel to perform hash join
__global__ void HashJoinKernel2(Record* s1Buf, Record* s2Buf, int* s1Partitions, int* s2Partitions, Record* dOutput, int* dOutputCount) {
  int s1Access = threadIdx.x + s1Partitions[blockIdx.x];
  int s1PartitionEnd = s1Partitions[blockIdx.x + 1];
  int s2Access = threadIdx.y + s2Partitions[blockIdx.x];
  int s2PartitionEnd = s2Partitions[blockIdx.x + 1];

  while(s1Access < s1PartitionEnd) {
    while(s2Access < s2PartitionEnd) {
      Record r1 = s1Buf[s1Access];
      Record r2 = s2Buf[s2Access];

      if(r1.a == r2.b) {
        Record newRecord;
        newRecord.a = r2.a;
        newRecord.b = r1.b;
        int storeIndex = atomicAdd(dOutputCount, 1);
        dOutput[storeIndex] = newRecord;
      }

      s2Access += blockDim.x;
    }
    s2Access = threadIdx.y + s2Partitions[blockIdx.x];
    s1Access += blockDim.x;
  }
}

GPUHashJoin2::GPUHashJoin2(Scan& s1, Scan& s2)
  : Join("GPU HASH JOIN 2", s1, s2)
{ }

void GPUHashJoin2::join(Scan& s1, Scan& s2)  {
  high_resolution_clock::time_point startPhase1 = high_resolution_clock::now();

  // Pull input data into buffers
  Record* s1Buffer = new Record[s1.getSize()];
  Record* s2Buffer = new Record[s2.getSize()];
  for(int i = 0; i < s1.getSize(); i++)
    s1Buffer[i] = s1.next();
  for(int i = 0; i < s2.getSize(); i++)
    s2Buffer[i] = s2.next();

  // Transfer input to device
  Record* s1Buf;
  Record* s2Buf;
  cudaMalloc(&s1Buf, s1.getSize() * sizeof(Record));
  cudaMalloc(&s2Buf, s2.getSize() * sizeof(Record));
  cudaMemcpy(s1Buf, s1Buffer, s1.getSize() * sizeof(Record), cudaMemcpyHostToDevice);
  cudaMemcpy(s2Buf, s2Buffer, s2.getSize() * sizeof(Record), cudaMemcpyHostToDevice);

  // Allocate buffers for partition indexes
  int numPartitions = std::max(s1.getSize(), s2.getSize()) / 32 + 1;
  int* s1Partitions = new int[numPartitions + 1];
  int* s2Partitions = new int[numPartitions + 1];
  int* s1PartitionsD;
  int* s2PartitionsD;
  cudaMalloc(&s1PartitionsD, (numPartitions + 1) * sizeof(int));
  cudaMalloc(&s2PartitionsD, (numPartitions + 1) * sizeof(int));

  int* curPartitionIdxS1;
  cudaMalloc(&curPartitionIdxS1, (numPartitions + 1) * sizeof(int));
  int* curPartitionIdxS2;
  cudaMalloc(&curPartitionIdxS2, (numPartitions + 1) * sizeof(int));

  // Allocate buffers for hash buffers (Output of PartitionInputKernel goes in these buffers)
  Record* s1Hash;
  Record* s2Hash;
  cudaMalloc(&s1Hash, s1.getSize() * sizeof(Record));
  cudaMalloc(&s2Hash, s2.getSize() * sizeof(Record));

  // Estimate output size and allocate output buffers
  int joinOutputSize = outputSize(s1Buffer, s2Buffer, s1.getSize(), s2.getSize());
  Record* dOutput;
  cudaMalloc(&dOutput, joinOutputSize * sizeof(Record));
  int outputCount;
  int* dOutputCount;
  cudaMalloc(&dOutputCount, sizeof(int));

  high_resolution_clock::time_point endPhase1 = high_resolution_clock::now();
  double duration_time_p2 = duration_cast<duration<double, std::milli>>(endPhase1 - startPhase1).count();
  #if TIMING
  std::cout << "Pulling input, memory allocation and data movement: " << duration_time_p2 << "ms" << std::endl;
  #endif
  high_resolution_clock::time_point hashPhaseStart = high_resolution_clock::now();

  // Call kernel to calculate number of elements in each hash bucket
  int numBlocksS1 = s1.getSize() / 32 + 1;
  int numBlocksS2 = s2.getSize() / 32 + 1;
  HashPartitionSizeKernel<<<numBlocksS1, 32>>>(s1Buf, s1.getSize(), s1PartitionsD, numPartitions, true);
  HashPartitionSizeKernel<<<numBlocksS2, 32>>>(s2Buf, s2.getSize(), s2PartitionsD, numPartitions, false);

  cudaDeviceSynchronize();

  cudaMemcpy(s1Partitions, s1PartitionsD, (numPartitions + 1) * sizeof(int), cudaMemcpyDeviceToHost);
  cudaMemcpy(s2Partitions, s2PartitionsD, (numPartitions + 1) * sizeof(int), cudaMemcpyDeviceToHost);

  // Compute partition arrays
  int s1PartitionSum = 0;
  for(int i = 0; i < numPartitions; i++) {
    int temp = s1Partitions[i];
    s1Partitions[i] = s1PartitionSum;
    s1PartitionSum += temp;
  }
  s1Partitions[numPartitions] = s1.getSize();
  int s2PartitionSum = 0;
  for(int i = 0; i < numPartitions; i++) {
    int temp = s2Partitions[i];
    s2Partitions[i] = s2PartitionSum;
    s2PartitionSum += temp;
  }
  s2Partitions[numPartitions] = s2.getSize();

  cudaMemcpy(s1PartitionsD, s1Partitions, (numPartitions + 1) * sizeof(int), cudaMemcpyHostToDevice);
  cudaMemcpy(s2PartitionsD, s2Partitions, (numPartitions + 1) * sizeof(int), cudaMemcpyHostToDevice);

  high_resolution_clock::time_point hashPhaseEnd = high_resolution_clock::now();
  double durationHashPhase = duration_cast<duration<double, std::milli>>(hashPhaseEnd - hashPhaseStart).count();
  #if TIMING
  std::cout << "Computing hash bucket sizes: " << durationHashPhase << "ms" << std::endl;
  #endif
  high_resolution_clock::time_point hash2PhaseStart = high_resolution_clock::now();

  cudaMemcpy(curPartitionIdxS1, s1PartitionsD, (numPartitions + 1) * sizeof(int), cudaMemcpyDeviceToDevice);
  cudaMemcpy(curPartitionIdxS2, s2PartitionsD, (numPartitions + 1) * sizeof(int), cudaMemcpyDeviceToDevice);

  // Call kernels to partition input buffers
  PartitionInputKernel<<<numBlocksS1, 32>>>(s1Buf, s1Hash, s1.getSize(), s1PartitionsD, curPartitionIdxS1, numPartitions, true);
  PartitionInputKernel<<<numBlocksS2, 32>>>(s2Buf, s2Hash, s2.getSize(), s2PartitionsD, curPartitionIdxS2, numPartitions, false);

  cudaDeviceSynchronize();

  high_resolution_clock::time_point hash2PhaseEnd = high_resolution_clock::now();
  double durationHashPhase2 = duration_cast<duration<double, std::milli>>(hash2PhaseEnd - hash2PhaseStart).count();
  #if TIMING
  std::cout << "Hash partitioning input buffers: " << durationHashPhase2 << "ms" << std::endl;
  #endif
  high_resolution_clock::time_point startKernel = high_resolution_clock::now();

  dim3 threadsPerBlock(32, 32);
  dim3 blocksInGrid(numPartitions);

  // Call kernel to perform hash join
  HashJoinKernel2<<<blocksInGrid, threadsPerBlock>>>(s1Hash, s2Hash, s1PartitionsD, s2PartitionsD, dOutput, dOutputCount);

  cudaDeviceSynchronize();

  high_resolution_clock::time_point endKernel = high_resolution_clock::now();
  double duration_time_kernel = duration_cast<duration<double, std::milli>>(endKernel - startKernel).count();
  #if TIMING
  std::cout << "Main kernel call: " << duration_time_kernel << "ms" << std::endl;
  #endif
  high_resolution_clock::time_point startLast = high_resolution_clock::now();

  cudaMemcpy(&outputCount, dOutputCount, sizeof(int), cudaMemcpyDeviceToHost);

  Record* output = new Record[outputCount];
  cudaMemcpy(output, dOutput, outputCount * sizeof(Record), cudaMemcpyDeviceToHost);

  // Return output
  for(int i = 0; i < outputCount; i++)
    push_next(output[i]);

  delete[] s1Buffer;
  delete[] s2Buffer;
  delete[] s1Partitions;
  delete[] s2Partitions;
  cudaFree(s1Buf);
  cudaFree(s2Buf);
  cudaFree(s1PartitionsD);
  cudaFree(s1PartitionsD);
  cudaFree(dOutput);

  high_resolution_clock::time_point endLast = high_resolution_clock::now();
  double duration_last = duration_cast<duration<double, std::milli>>(endLast - startLast).count();
  #if TIMING
  std::cout << "Pushing output and freeing memory: " << duration_last << "ms" << std::endl;
  std::cout << "Execution Time: " << durationHashPhase + durationHashPhase2 +  duration_time_kernel << "ms." << std::endl;
  #endif
}
