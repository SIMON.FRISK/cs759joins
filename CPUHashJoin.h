#pragma once
#include "defs.h"
#include "Scan.h"
#include "Join.h"

class CPUHashJoin : public Join {
public:
  CPUHashJoin(Scan& s1, Scan& s2);
  void join(Scan& s1, Scan& s2);
};
