#pragma once
#include "defs.h"
#include <list>

class HashMap {
  public:
    HashMap(int partitions);
    ~HashMap();
    void add(int key, Record record);
    std::list<Record> get(int key);
    void toMemoryBuffer(Record* buffer, int* splits);
  private:
    int _partitions;
    std::list<Record>* _table;
};
