#pragma once
#include "defs.h"
#include <random>

class Scan {
public:
  Scan(int tableSize, int seed);
  Record next();
  int getSize() { return _tableSize; }
private:
  int _created;
  int _tableSize;
  // Random number generation
  std::mt19937 _random;
  std::uniform_int_distribution<int> _generate;
};
