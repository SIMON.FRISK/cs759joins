#pragma once
#include "defs.h"
#include "Scan.h"
#include "Join.h"

class GPUHashJoin1 : public Join {
public:
  GPUHashJoin1(Scan& s1, Scan& s2);
  virtual void join(Scan& s1, Scan& s2);
};
