#pragma once
#include "defs.h"
#include "Scan.h"
#include "Join.h"

class GPULoopsJoin : public Join {
public:
  GPULoopsJoin(Scan& s1, Scan& s2);
  virtual void join(Scan& s1, Scan& s2);
};
